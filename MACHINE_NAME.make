; Drupal definitions
core = 7.x
api = 2

; Translations
translations[] = fr

; Drupal core
projects[drupal][version] = 7.22

; Development modules
projects[admin_menu][version] = 3.0-rc4

projects[devel][version] = 1.3

projects[diff][version] = 3.2

projects[module_filter][version] = 1.7

; Export modules
projects[features][version] = 2.0-beta2

projects[features_extra][version] = 1.0-beta1

projects[strongarm][version] = 2.0

projects[uuid_features][version] = 1.0-alpha3

; API modules
projects[ctools][version] = 1.3

projects[entity][version] = 1.1

projects[libraries][version] = 2.1

projects[token][version] = 1.5

projects[uuid][version] = 1.0-alpha4

; Content modules
projects[views][version] = 3.7

; Other modules
projects[better_formats][version] = 1.0-beta1

projects[ckeditor][version] = 1.13

projects[imce][version] = 1.7

projects[jquery_update][version] = 2.3

projects[linkit][version] = 2.6

; Themes
projects[adaptivetheme][type] = theme
projects[adaptivetheme][download][type] = file
projects[adaptivetheme][download][url] = http://ftp.drupal.org/files/projects/adaptivetheme-7.x-3.x-dev.tar.gz

projects[bootstrap][type] = theme
projects[bootstrap][download][type] = git
projects[bootstrap][download][url] = http://git.drupal.org/project/bootstrap.git
projects[bootstrap][download][branch] = 7.x-2.x
projects[bootstrap][download][revision] = e426a14c1617361532d48ad669f3fda22f51dd38

; Libraries
libraries[ckeditor][download][type] = file
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.1/ckeditor_4.1_standard.zip
