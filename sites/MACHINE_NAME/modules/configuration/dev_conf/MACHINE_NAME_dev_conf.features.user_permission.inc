<?php
/**
 * @file
 * {MACHINE_NAME}_dev_conf.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function {MACHINE_NAME}_dev_conf_user_default_permissions() {
  $permissions = array();

  // Exported permission: switch users.
  $permissions['switch users'] = array(
    'name' => 'switch users',
    'roles' => array(
      'Super administrator' => 'Super administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'devel',
  );

  return $permissions;
}
