<?php
/**
 * @file
 * {MACHINE_NAME}_dev_conf.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function {MACHINE_NAME}_dev_conf_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['devel-switch_user'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'switch_user',
    'module' => 'devel',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bootstrap' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
