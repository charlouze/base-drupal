<?php
/**
 * @file
 * {MACHINE_NAME}_input_formats.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function {MACHINE_NAME}_input_formats_user_default_permissions() {
  $permissions = array();

  // Exported permission: use text format ckeditor_full.
  $permissions['use text format ckeditor_full'] = array(
    'name' => 'use text format ckeditor_full',
    'roles' => array(
      'Super administrator' => 'Super administrator',
    ),
    'module' => 'filter',
  );

  // Exported permission: use text format ckeditor_light.
  $permissions['use text format ckeditor_light'] = array(
    'name' => 'use text format ckeditor_light',
    'roles' => array(
      'Super administrator' => 'Super administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'filter',
  );

  return $permissions;
}
