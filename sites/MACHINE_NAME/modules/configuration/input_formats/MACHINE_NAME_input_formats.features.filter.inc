<?php
/**
 * @file
 * {MACHINE_NAME}_input_formats.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function {MACHINE_NAME}_input_formats_filter_default_formats() {
  $formats = array();

  // Exported format: CKEditor Full.
  $formats['ckeditor_full'] = array(
    'format' => 'ckeditor_full',
    'name' => 'CKEditor Full',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  // Exported format: CKEditor Light.
  $formats['ckeditor_light'] = array(
    'format' => 'ckeditor_light',
    'name' => 'CKEditor Light',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(
      'filter_html' => array(
        'weight' => -10,
        'status' => 1,
        'settings' => array(
          'allowed_html' => '<strong> <em> <u> <blockquote> <ul> <ol> <li> <a>',
          'filter_html_help' => 1,
          'filter_html_nofollow' => 0,
        ),
      ),
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
    ),
  );

  return $formats;
}
