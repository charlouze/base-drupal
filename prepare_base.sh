#!/bin/bash

function register_var() {
  read -p "$2: " "$1"
  echo "s/{$1}/"${!1}"/g" >> $SED_SCRIPT_FILE
}

function sed_var() {
  sed -f $SED_SCRIPT_FILE $1 > tmpfile
  mv tmpfile $1

  if [ "x$2" != "x" ]
  then
    mv $1 $2
  fi
}

function handle_folder() {
  for file in $1/*
  do
    dirname=$(dirname $file)
    oldFilename=$(basename $file)
    newFilename=$MACHINE_NAME${oldFilename:12}

    sed_var "$dirname/$oldFilename" "$dirname/$newFilename"
  done
  
  if [ "x$2" != "x" ]
  then
    mv $1 $2
  fi
}

SED_SCRIPT_FILE="sed_script_file"

register_var MACHINE_NAME "Machine name of the site (db name, profile name, etc...)"
register_var FULL_NAME "Full name of the site"
register_var DESCRIPTION "Slogan of the site"
register_var ACCOUNT_NAME "Account name of the site super admin"
register_var ACCOUNT_MAIL "Account email of the site super admin"

sed_var install.sh
chmod +x install.sh

sed_var fix_perms.sh
chmod +x fix_perms.sh

sed_var sites/sites.php

sed_var .gitignore

handle_folder profiles/MACHINE_NAME profiles/$MACHINE_NAME

handle_folder sites/MACHINE_NAME/modules/configuration/base_configuration

handle_folder sites/MACHINE_NAME/modules/configuration/dev_conf

handle_folder sites/MACHINE_NAME/modules/configuration/fields_bases

handle_folder sites/MACHINE_NAME/modules/configuration/input_formats

mv sites/MACHINE_NAME sites/$MACHINE_NAME
mv MACHINE_NAME.make $MACHINE_NAME.make

rm sed_script_file
rm prepare_base.sh
