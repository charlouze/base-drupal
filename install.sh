#!/bin/bash

NAME="{MACHINE_NAME}"
SITE_DIR="sites/$NAME"
SETTINGS_FILE="$SITE_DIR/settings.php"

ACCOUNT_MAIL="{ACCOUNT_MAIL}"
ACCOUNT_NAME="{ACCOUNT_NAME}"

function db_url() {
    if [ "x$1" != "x" ]
    then
        DB_URL="$DB_URL$2"
    else
        DB_URL="$DB_URL$3"
    fi
}

if [ -f "$SETTINGS_FILE" ]
then
    chmod 755 "$SITE_DIR"
    chmod 644 "$SETTINGS_FILE"
    rm -f "$SETTINGS_FILE"
fi

DB_URL="mysql://"

db_url "$MYSQL_USER" "$MYSQL_USER" "root"

db_url "$MYSQL_PASS" ":$MYSQL_PASS" ""

db_url "$MYSQL_HOST" "@$MYSQL_HOST" "@localhost"

db_url "$MYSQL_PORT" ":$MYSQL_PORT" ":3306"

DB_URL="$DB_URL/$NAME"

drush -y site-install $NAME --account-mail=$ACCOUNT_MAIL  --account-name=$ACCOUNT_NAME --account-pass=azertyuiop --sites-subdir=$NAME --db-url=$DB_URL --locale=en

# features update
drush -y -l $NAME fra

# cache clear
drush -y -l $NAME cache-clear all

